package models;

public class MovableCircle  implements Movable {

    public int radius;
    public MovablePoint  center;
    public  MovableCircle(int x1, int y1, int xSpeed1, int ySpeed1, int radius) {
        this.center = new MovablePoint(x1, y1, xSpeed1, ySpeed1);
       

        this.radius = radius;
       
    }
    @Override
    public String toString() {
        return "MovablePoint [x=" + center.x + ", y=" + center.y + "]" +  "Speed [x=" + center.xSpeed + ", y=" + center.ySpeed +"]" + ", Radius = [" + radius + "]";
    }


    public void moveUp() {
        center.x += center.xSpeed;

    }

    public void moveDown() {
        center.x -= center.xSpeed;

    }
    public void moveLeft() {
        center.y += center.ySpeed;

    }
    public void moveRight() {
        center.y -= center.ySpeed;

    }
    public MovablePoint getCenter() {
        return center;
    }
    
    

    
}