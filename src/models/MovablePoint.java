package models;

public class MovablePoint implements Movable {
    int x;
    int y;
    int xSpeed;  // dịch chuyển nhiều đơn vi x thay vì 1 đơn vị
    int ySpeed;
    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }
    @Override
    public String toString() {
        return "MovablePoint [x=" + x + ", y=" + y + "]" +  "Speed [x=Speed" + xSpeed + ", ySpeed=" + ySpeed +"]";
    }
   
    public void moveUp() {
        this.x += xSpeed;

    }

    public void moveDown() {
        this.x -= xSpeed;

    }
    public void moveLeft() {
        this.y += ySpeed;

    }
    public void moveRight() {
        this.y -= ySpeed;

    }
    

    

    

    
}
